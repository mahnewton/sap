
import os
import sys
import math
import numpy as np
import tensorflow as tf
import SAP_Preprocessing as prep

from keras import backend as K
from keras import optimizers
from keras.optimizers import SGD
from sklearn.utils import class_weight
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Input
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import mean_absolute_error
from keras.models import model_from_json
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


ProteinName = sys.argv[1]

LenFeatures, WinSize, LenOut = 35, 2, 4
LenInputNN = (2*WinSize+1) * LenFeatures



# Directories
DataFolder = "../inputs/"
BuffFolder = "../data/"
NNFile = BuffFolder + "NN_SAP.json"
WFile = BuffFolder + "W_SAP.hdf5"
MinMaxFile = BuffFolder + "MinMeanMaxSTD_SAP.txt"


# Reading Data
MinMean, MaxSTD = prep.ExtractMinMax(MinMaxFile)
Proteins = [ProteinName]

				
# Pre-processing
TestInput, TestOutput = prep.SAPPreprocessing(DataFolder, Proteins, LenFeatures, LenInputNN, LenOut, WinSize, MinMean, MaxSTD)
	


# Testing
json_file = open(NNFile, 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights(WFile)


PredTestOutput = loaded_model.predict(TestInput)
PredTestOutput = prep.ShiftingAngle(PredTestOutput)

# Print predictions
print("# Phi Psi Theta Tau")
for i in range(PredTestOutput.shape[0]):
	print("%.2f" % PredTestOutput[i, 0], "%.2f" % PredTestOutput[i, 1], "%.2f" % PredTestOutput[i, 2], "%.2f" % PredTestOutput[i, 3])
# Printing MAE
prep.PrintMAE(TestOutput, PredTestOutput)






































































