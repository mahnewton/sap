
import numpy as np

AngleMaskValue =360
LenSSHotvect = 8



PhyscDict = {'A': [-0.350, -0.680, -0.677, -0.171, -0.170, 0.900, -0.476], 'C': [-0.140, -0.329, -0.359, 0.508, -0.114, -0.652, 0.476], 'D': [-0.213, -0.417, -0.281, -0.767, -0.900, -0.155, -0.635], 'E': [-0.230, -0.241, -0.058, -0.696, -0.868, 0.900, -0.582], 'F': [ 0.363, 0.373, 0.412, 0.646, -0.272, 0.155, 0.318], 'G': [-0.900, -0.900, -0.900, -0.342, -0.179, -0.900, -0.900], 'H': [ 0.384, 0.110, 0.138, -0.271, 0.195, -0.031, -0.106], 'I': [ 0.900, -0.066, -0.009, 0.652, -0.186, 0.155, 0.688], 'K': [-0.088, 0.066, 0.163, -0.889, 0.727, 0.279, -0.265], 'L': [ 0.213, -0.066, -0.009, 0.596, -0.186, 0.714, -0.053], 'M': [ 0.110, 0.066, 0.087, 0.337, -0.262, 0.652, -0.001], 'N': [-0.213, -0.329, -0.243, -0.674, -0.075, -0.403, -0.529], 'P': [ 0.247, -0.900, -0.294, 0.055, -0.010, -0.900, 0.106], 'Q': [-0.230, -0.110, -0.020, -0.464, -0.276, 0.528, -0.371], 'R': [ 0.105, 0.373, 0.466, -0.900, 0.900, 0.528, -0.371], 'S': [-0.337, -0.637, -0.544, -0.364, -0.265, -0.466, -0.212], 'T': [ 0.402, -0.417, -0.321, -0.199, -0.288, -0.403, 0.212], 'V': [ 0.677, -0.285, -0.232, 0.331, -0.191, -0.031, 0.900], 'W': [ 0.479, 0.900, 0.900, 0.900, -0.209, 0.279, 0.529], 'Y': [ 0.363, 0.417, 0.541, 0.188, -0.274, -0.155, 0.476]}

HotvectDict = {'B' : [1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0], 'C' : [0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0], 'E' : [0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0], 'G' : [0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0], 'H' : [0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0], 'I' : [0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0], 'S' : [0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0], 'T' : [0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0]}


# Functions
def ExtractPhiPsi(DSSPFile):
	with open(DSSPFile) as p:
		Lines = p.readlines()
		p.close()
	Phi=np.array([float(i) for i in Lines[3].strip().split(' ')])
	Psi=np.array([float(i) for i in Lines[4].strip().split(' ')])
	Phi = Phi.reshape(-1, 1)
	Psi = Psi.reshape(-1, 1)
	return  Phi, Psi



def ExtractAA(DSSPFile):
	with open(DSSPFile) as p:
		Lines = p.readlines()
		p.close()
	AA = Lines[1].strip()
	return AA




def ExtractSSPredicted(SSPro8File):
	with open(SSPro8File) as p:
		Lines = p.readlines()
		p.close()
	SSPred = Lines[1].strip()
	return SSPred




def ExtractPSSMMtx(PSSMFile):
	PSSMMtx = np.zeros((0,20))
	with open(PSSMFile) as p:
		Lines = p.readlines()
		p.close()
	for Line in Lines:
		SplitLine = Line.strip().split()
		if (len(SplitLine) == 44) and (SplitLine[0] != '#'):			
			PSSMRow = np.array([-float(i) for i in SplitLine[2:22]]).reshape(1, -1)
			PSSMMtx = np.concatenate((PSSMMtx, PSSMRow), axis=0)

	return PSSMMtx




def ExtractThetaTau(TFile):
	with open(TFile) as p:
		Lines = p.readlines()
		Theta, Tau = [], []
		for Line in Lines:		
			SplitLine = Line.strip().split()
			if (len(SplitLine) != 0 and len(SplitLine[0]) != 0):
				if (SplitLine[0][0] != '#'):				
					Theta.append(float(SplitLine[2]))
					Tau.append(float(SplitLine[3]))
		p.close()
	Theta = np.array(Theta).reshape(-1,1)
	Tau = np.array(Tau).reshape(-1,1)
	return Theta, Tau




def Normalization(InputMtx, MinMean, MaxSTD):
	for i in range (0, InputMtx.shape[1]):
		InputMtx[:,i] = (InputMtx[:, i] - float(MinMean[0,i]))/((float(MaxSTD[0,i])-float(MinMean[0,i])))
	return InputMtx



def WindowSliding(protein_name, win_step , win_size ,input_matrix, len_feature_vect ):
	len_input_nn= (2*win_size+1)*len_feature_vect
	len_prot = input_matrix.shape[0]
	window_input_mtx=np.zeros([0,len_input_nn])
	window_whole_prot = np.zeros([0,len_input_nn])
	for core in range (0, len_prot, 1):
		left_win = core - win_size
		right_win = core + win_size
		if (right_win >= len_prot ):
			right_win = right_win-len_prot
			window_input_mtx = np.concatenate(( input_matrix[left_win:, :].ravel() ,input_matrix[:right_win+1, :].ravel() ), axis=0)
			window_input_mtx = window_input_mtx.reshape (1, window_input_mtx.shape[0])
		elif (left_win < 0):
			window_input_mtx = np.concatenate(( input_matrix[left_win:, :].ravel() ,input_matrix[:right_win+1, :].ravel()  ), axis=0)
			window_input_mtx = window_input_mtx.reshape (1, window_input_mtx.shape[0])
		else:
			window_input_mtx = input_matrix[left_win:right_win+1, :] 
			window_input_mtx = window_input_mtx.ravel()
			window_input_mtx = window_input_mtx.reshape (1, window_input_mtx.shape[0])
		if (window_whole_prot.shape[1] != window_input_mtx.shape[1]):
			print(window_whole_prot.shape, window_input_mtx.shape)
			print(protein_name)
			print(len_prot, left_win, right_win, core)
		window_whole_prot =  np.concatenate((window_whole_prot, window_input_mtx ), axis=0)
	return window_whole_prot




def ConvertSS2Hotvect(SSPred):
	SS8HotMtx = np.zeros((0,LenSSHotvect))
	for i in range (len(SSPred)):
		SS8HotRow = np.array(HotvectDict[SSPred[i]]).reshape(1,LenSSHotvect)
		SS8HotMtx = np.concatenate((SS8HotMtx, SS8HotRow), axis=0)
	return SS8HotMtx




def ExtractPhysc(AA):
	PhyscMtx = np.zeros((0,7))
	for i in range(len(AA)):
		PhyscRow = np.array(PhyscDict[AA[i]]).reshape(1, -1)
		PhyscMtx = np.concatenate((PhyscMtx, PhyscRow), axis=0)
	return PhyscMtx




def SAPPreprocessing(DataFolder, Proteins, LenFeatures, LenInputNN, LenOut, WinSize, MinMean, MaxSTD):
	
	FinalInputs = np.zeros((0, LenInputNN))
	FinalOutputs = np.zeros((0, LenOut))
	for Protein in Proteins:
		ProteinsFolder = DataFolder + "proteins/" + Protein + "/"
		PSSMFile = ProteinsFolder + Protein + ".pssm"
		DSSPFile = ProteinsFolder + Protein + ".dssp"
		TFile = ProteinsFolder + Protein + ".t"
		SS8ProFile = ProteinsFolder + Protein + ".out.ss8"
	
		Theta, Tau = ExtractThetaTau(TFile)
		Phi, Psi = ExtractPhiPsi(DSSPFile)
		AA = ExtractAA(DSSPFile)
		SS8Pred = ExtractSSPredicted(SS8ProFile)
		PSSMMtx = ExtractPSSMMtx(PSSMFile)
		SS8Hotvext = ConvertSS2Hotvect(SS8Pred)
		PhyscMtx = ExtractPhysc(AA)

		InputFeaturesMtx = np.concatenate((PSSMMtx, PhyscMtx, SS8Hotvext), axis=1 )
		FeaturesWindowed = WindowSliding(Protein, 1, WinSize, InputFeaturesMtx, LenFeatures)

		OutAngles =  np.concatenate((Phi, Psi, Theta, Tau), axis=1 )

		FinalInputs = np.concatenate ((FinalInputs, FeaturesWindowed), axis=0)
		FinalOutputs = np.concatenate ((FinalOutputs, OutAngles), axis=0)


	FinalInputs = Normalization(FinalInputs, MinMean, MaxSTD)
	return FinalInputs, FinalOutputs



def ExtractMinMax(MinMaxFile):
	with open(MinMaxFile) as P:
		Lines = P.readlines()
		P.close()
		MinMean, MaxSTD = [], []
		for Line in Lines:
			SplitLine=Line.strip().split()			
			if SplitLine[0][0] != "#":
				MinMean.append(float(SplitLine[0]))
				MaxSTD.append(float(SplitLine[1]))
		MinMean = np.array(MinMean).reshape(1, -1)
		MaxSTD = np.array(MaxSTD).reshape(1, -1)
	return MinMean, MaxSTD







def ShiftingAngle(InputMtx):
	ShiftedInputMtx = InputMtx
	for i in range(0, InputMtx.shape[0]):
		for j in range(0, InputMtx.shape[1]):
			if (InputMtx[i, j] > 180):
				ShiftedInputMtx[i, j] = InputMtx[i, j] - 360
			elif (InputMtx[i, j] <= -180):
				ShiftedInputMtx[i, j] = InputMtx[i, j] + 360
	return ShiftedInputMtx











def PrintMAE(TestOutput, PredTestOutput):
	PhiErr = CalAEAngles(TestOutput[:, 0:1], PredTestOutput[:, 0:1])
	PsiErr = CalAEAngles(TestOutput[:, 1:2], PredTestOutput[:, 1:2])
	ThetaErr = CalAEAngles(TestOutput[:, 2:3], PredTestOutput[:, 2:3])
	TauErr = CalAEAngles(TestOutput[:, 3:4], PredTestOutput[:, 3:4])

	print("# ===================================")
	print("# MAE for:			")
	print ("# Phi      Psi      Theta     Tau")
	print("# -----------------------------------")
	print("# %.2f" % PhiErr, "  ", "%.2f" % PsiErr, "  ", "%.2f" % ThetaErr, "    ", "%.2f" % TauErr)
	print("# ===================================")

	return






def CalAEAngles(NativeAngle, PredAngle):
	Mask = np.zeros(NativeAngle.shape)
	for i in range (0, NativeAngle.shape[0]):
		for j in range (0, NativeAngle.shape[1]):
			if (NativeAngle[i,j] != AngleMaskValue):
				Mask[i,j] = 1.0
	Native = NativeAngle * Mask
	Pred = PredAngle * Mask
	Err = abs(Pred - Native)


	for i in range (0, Err.shape[0]):
		for j in range (0, Err.shape[1]):
			if (Err[i,j] >= 180):
				Err[i,j] = abs(360 - Err[i,j])


	return np.sum(Err)/np.sum(Mask)









