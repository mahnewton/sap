#!/bin/bash

# run this program from the code folder

input_protein_list=../inputs/port_list.txt


while IFS= read -r line
do
	python ./SAP.py ${line} > ../outputs/${line}.sap

done < "${input_protein_list}"




