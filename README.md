# Simpler Angle Predictor (SAP)

This project provides the programs and related files of SAP that predicts protein backbone angles phi, psi, theta, and tau. 

# Correction

Note that after the paper is published, we have found some programming bugs that affect the accuracy numbers reported in the paper. However, the generalised conclusions hold. Consequently, the programs provided here have been updtaed. We have notified the Scientific Reports journal about this issue and going through their due process. Meanwhile, should you have any queries, please do not hesitate to contact us by email. 


## Requirements

To run the program, for each protein named **protein_name**, you need the following files

- protein_name.fasta
- protein_name.dssp
- protein_name.pssm
- protein_name.t
- protein_name.out.ss8
 
All of these files must be in the inputs folder.

## Getting Input Files

The first four file are available from the SPOT-1D dataset and to get the .out.ss8 file, visit 

the website "http://download.igb.uci.edu/" and download SCRATCH-1D release 1.2 package. Then 

Run SCRATCH-1D on your dataset. It need two inputs: the fasta file of a protein and the name of the output file.


    	../bin/run_SCRATCH-1D_predictors.sh protein_name.fasta protein_name.out 


## Running the Program

- Update the prot_list.txt with the names of the protein files
- Put the protein data for each protein in a separate folder inside the inputs folder
- Run **bash RunSAP.sh** from the **code** folder
- The outputs are generated in the outputs folder: .sap file for each protein





